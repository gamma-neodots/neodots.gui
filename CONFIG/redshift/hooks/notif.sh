#!/usr/bin/env bash
case $1 in
	period-changed) exec dunstify --icon=/dev/null --appname=Redshift "${3^}" "${2^} ⇒ ${3^}" ;;
	*) exec echo "$@" | ts > "$HOME/.redshift.log" ;;
esac
