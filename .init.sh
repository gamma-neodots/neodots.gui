#!/usr/bin/env bash
ldir "CONFIG" "${XDG_CONFIG_HOME:-"$HOME/.config"}"
ldir "SHARE" "${XDG_CONFIG_HOME:-"$HOME/.local/share"}"
ldir "HOME" "$HOME"
grep Path "$HOME/.mozilla/firefox/profiles.ini" | cut -d '=' -f 2 | while read -r ffprofile; do
	ldir "FIREFOX" "$HOME/.mozilla/firefox/$ffprofile"
done
case "$1" in
debugi) ;&
install)
	grep -q "#include \"$(pwd)/Xresources\"" "$HOME/.Xresources" || echo "#include \"$(pwd)/Xresources\"" >> "$HOME/.Xresources"
	;;
esac
